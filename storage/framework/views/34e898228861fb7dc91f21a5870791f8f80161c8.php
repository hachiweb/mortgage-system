<!DOCTYPE html>
<html lang="en">

<head>
    <title>Mortgage System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">

</head>

<body>
    <div class="container_width">
        <div class="row">
            <div class="col-md-3 left_bg_color">
                <form>
                    <div class="left_section">
                        <div class="left_section_heading">
                            <h2>Refine results</h2>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="Mortgages" value="Rocket Mortgages" checked>Rocket Mortgages
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="Mortgages" value="Premium Mortgages" >Premium Mortgages
                            </label>
                        </div>
                        <div class="left_label_text">
                            <label for="">Country</label>
                            <select name="" id="country" class="form-control">
                                <option value="Canada">Canada</option>
                            </select>
                        </div>
                        <div class="left_label_text">
                            <label for="">Credit Score</label>
                            <select name="" id="credit_score" class="form-control">
                                <option value="720 or above">720 or above</option>
                            </select>
                        </div>
                        <div class="left_label_text">
                            <label for="">Purchase Price</label>
                            <select name="" id="purchase_price" class="form-control">
                                <option value="Minimum 10k+">Minimum 10k+</option>
                            </select>
                        </div>
                        <div class="left_label_text">
                            <label for="">Down Payment</label>
                            <select name="" id="down_payment" class="form-control">
                                <option value="Minimum of 10% of purchase price">Minimum of 10% of purchase price
                                </option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-9">
                <div class="right_section">
                    <div class="advertiser-disclosure">
                        <!-- <button class="btn btn-default" type="button">Advertiser disclosure</button> -->
                        <button id="exampleButton" type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="top" data-content="We believe everyone should be able to make financial decisions with confidence. And while our site doesn’t feature every company or financial product available on the market, we’re proud that the guidance we offer, the information we provide and the tools we create are objective, independent, straightforward — and free.">Advertiser disclosure</button>
                    </div>
                    <div class="top_heading">
                        <h1>Your personalized rates</h1>
                        <h6>Shop the latest mortgage and refinance rates and get quotes tailored to you.</h6>
                    </div>
                    <div class="fixed_loan_heading">
                        <h4>30-year fixed loan rate options</h4>
                        <a href="">
                            <p>LICENSE INFORMATION</p>
                        </a>
                    </div>
                    <div class="sort_by">
                        <label for="">Short By</label>
                        <select id="" name="" class="">
                            <option value="interestRate">Interest Rate</option>
                            <option value="payment.total">Monthly payment</option>
                            <option value="fees.total">Fees</option>
                            <option value="trueCostStats.accInterest">Total interest</option>
                        </select>
                    </div>
                    <div class="right_sec_top_box">
                        <div class="half_top_sec">
                            <div class="half_top_sec_logo">
                                <img src="<?php echo e(asset('images/BettercomLogoNEW.svg')); ?>" alt="">
                                <p>Better.com: NMLS#330511</p>
                            </div>
                            <div class="button_div">
                                <button type="button" class="btn right_sec_btn">Explore quote</button>
                                <p>with NerdWallet</p>
                            </div>
                        </div>
                        <div class="right_sec_bottom_box">
                            <div class="row">
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>INTEREST RATE</p>
                                        <h5><span>3.5</span>%</h5>
                                        <!-- <div class="loader">Loading...</div> -->
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>MO. PAYMENT</p>
                                        <h5>$<span>988</span> </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>TOTAL INTEREST</p>
                                        <h5>$<span>135,643</span></h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>FEES</p>
                                        <h5>$<span>571</span></h5>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                    <div class="right_sec_top_box">
                        <div class="half_top_sec">
                            <div class="half_top_sec_logo">
                                <img src="<?php echo e(asset('images/NAFLogo.svg')); ?>" alt="">
                                <p>New American Funding: NMLS#6606</p>
                            </div>
                            <div class="button_div">
                                <button type="button" class="btn right_sec_btn">Explore quote</button>
                                <p>(888) 978-8131</p>
                            </div>
                        </div>
                        <div class="right_sec_bottom_box">
                            <div class="row">
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>INTEREST RATE</p>
                                        <h5><span>3.5</span>%</h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>MO. PAYMENT</p>
                                        <h5>$<span>988</span> </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>TOTAL INTEREST</p>
                                        <h5>$<span>135,643</span></h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>FEES</p>
                                        <h5>$<span>571</span></h5>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                    <div class="right_sec_top_box">
                        <div class="half_top_sec">
                            <div class="half_top_sec_logo">
                                <img src="<?php echo e(asset('images/Reali-Loans-Hor-Lockup.png')); ?>" alt="">
                                <p>Reali: NMLS#991397</p>
                            </div>
                            <div class="button_div">
                                <button type="button" class="btn right_sec_btn">Explore quote</button>
                            </div>
                        </div>
                        <div class="right_sec_bottom_box">
                            <div class="row">
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>INTEREST RATE</p>
                                        <h5><span>3.5</span>%</h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>MO. PAYMENT</p>
                                        <h5>$<span>988</span> </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>TOTAL INTEREST</p>
                                        <h5>$<span>135,643</span></h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-6">
                                    <div class="intrest_rate">
                                        <p>FEES</p>
                                        <h5>$<span>571</span></h5>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                    <p class="about_rate">About These Rates: The lenders whose rates appear on this table are
                        NerdWallet’s advertising partners. NerdWallet strives to keep its information accurate and up to
                        date. This information may be different than what you see when you visit a lender’s site. The
                        terms advertised here are not offers and do not bind any lender. The rates shown here are
                        retrieved via the Mortech rate engine and are subject to change. These rates do not include
                        taxes, fees, and insurance. Your actual rate and loan terms will be determined by the partner’s
                        assessment of your creditworthiness and other factors. Any potential savings figures are
                        estimates based on the information provided by you and our advertising partners.</p>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="mortgage_rate">
                                <h2>Mortgage rate trends (APR)</h2>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="mortgage_rate_insight">
                                <h5>NerdWallet’s mortgage rate insight</h5>
                            </div>
                            <div class="fixed_year">
                                <h4>3.157%</h4>
                                <h5>30-year fixed</h5>
                            </div>
                            <p class="fixed_year_text">On Friday, Aug. 14, 2020, the average rate on a 30-year
                                fixed-rate mortgage rose one basis point to 3.157%, the average rate on a 15-year
                                fixed-rate mortgage went up three basis points to 2.732% and the average rate on a 5/1
                                ARM was unchanged at 3.024%, according to a NerdWallet survey of mortgage rates
                                published daily by national lenders. A basis point is one one-hundredth of one percent.
                                Rates are expressed as annual percentage rate, or APR. The 30-year fixed-rate mortgage
                                is 11 basis points higher than one week ago and 84 basis points lower than one year ago.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="mortgage_rate">
                                <h2>Current mortgage and refinance rates</h2>
                                <p>Accurate as of 08/14/2020.</p>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Interest rate</th>
                                            <th>APR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="">
                                            <td class="">30-year fixed rate</td>
                                            <td class="">3.044%</td>
                                            <td class="">3.157%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">20-year fixed rate</td>
                                            <td class="">2.875%</td>
                                            <td class="">3.082%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">15-year fixed rate</td>
                                            <td class="">2.536%</td>
                                            <td class="">2.732%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">5/1 ARM rate</td>
                                            <td class="">3.188%</td>
                                            <td class="">3.024%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">7/1 ARM rate</td>
                                            <td class="">2.750%</td>
                                            <td class="">2.930%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">10/1 ARM rate</td>
                                            <td class="">2.875%</td>
                                            <td class="">3.013%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">30-year fixed FHA rate</td>
                                            <td class="">3.750%</td>
                                            <td class="">4.810%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">30-year fixed VA rate</td>
                                            <td class="">3.188%</td>
                                            <td class="">3.537%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">30-year fixed jumbo rate</td>
                                            <td class="">3.250%</td>
                                            <td class="">3.284%</td>
                                        </tr>
                                        <tr class="">
                                            <td class="">15-year fixed jumbo rate</td>
                                            <td class="">2.875%</td>
                                            <td class="">2.990%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-5">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="faq">
                                <div id="accordion" class="accordion">
                                    <div class="card mb-0">
                                        <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                                            <a class="card-title">
                                            What are today’s mortgage rates?
                                            </a>
                                        </div>
                                        <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                                            <p>For today, August 14th, 2020, the current average mortgage rate on the 30-year fixed-rate mortgage is 3.157%, the average rate for the 15-year fixed-rate mortgage is 2.732%, and the average rate on the 5/1 adjustable-rate mortgage (ARM) is 3.024%. Rates are quoted as annual percentage rate (APR).
                                            </p>
                                        </div>
                                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            <a class="card-title">
                                            How do I compare current mortgage rates?
                                            </a>
                                        </div>
                                        <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                                            <p>The more lenders you check out when shopping for mortgage rates, the more likely you are to get a lower interest rate. Getting a lower interest rate could save you hundreds of dollars over a year of mortgage payments — and thousands of dollars over the life of the mortgage.</br>
                                            With NerdWallet's easy-to-use mortgage rate tool, you can compare current home loan interest rates — whether you're a first-time home buyer looking at 30-year fixed mortgage rates or a longtime homeowner comparing refinance mortgage rates.
                                            </p>
                                        </div>
                                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            <a class="card-title">
                                            How do I find personalized mortgage rates?
                                            </a>
                                        </div>
                                        <div id="collapseThree" class="collapse" data-parent="#accordion" >
                                            <div class="card-body">
                                                <p>At a high level, mortgage rates are determined by economic forces that influence the bond market. You can't do anything about that, but it's worth knowing: bad economic or global political worries can move mortgage rates lower. Good news can push rates higher.</br>
                                                What you can control are the amount of your down payment and your credit score. Lenders fine-tune their base interest rate on the risk they perceive to be taking with an individual loan.</br>
                                                So their base mortgage rate, computed with a profit margin aligned with the bond market, is adjusted higher or lower for each loan they offer. Higher mortgage rates for higher risk; lower rates for less perceived risk.</br>
                                                So the bigger your down payment and the higher your credit score, generally the lower your mortgage rate.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mortgage_rate_averages">
                                <h2 class="mortgage_rate_averages-heading">Why you can trust NerdWallet’s mortgage rate averages</h2>
                                <p>To make financial decisions with confidence, you need information you can trust. NerdWallet’s mortgage rate averages are trustworthy because they are independent of any business relationships with lenders. NerdWallet gathers mortgage rates daily from national and regional lenders on consumer-facing websites. To see which lenders NerdWallet gathers rates from, and which assumptions we use, see the
                                <a href="">NerdWallet Mortgage Rate Index methodology.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mortgage_rate_averages">
                                <h2 class="mortgage_rate_averages-heading">Check out our other mortgage and refinance tools</h2>
                               <div class="row">
                                   <div class="col-md-3">
                                       <div class="tools">
                                           <h6>LENDERS</h6>
                                           <div class="main_center">
                                                <img src="<?php echo e(asset('images/tool_img1.png')); ?>" alt="tool image">
                                                <h3>Get pre-approved</h3>
                                           </div>
                                           <p>Get your true budget and find a home with ease.</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3">
                                       <div class="tools">
                                           <h6>AGENTS</h6>
                                           <div class="main_center">
                                                <img src="<?php echo e(asset('images/tool_img2.png')); ?>" alt="tool image">
                                                <h3>Find a real estate agent</h3>
                                           </div>
                                           <p>Get matched with a top agent in your area.</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3">
                                       <div class="tools">
                                           <h6>CALCULATOR</h6>
                                           <div class="main_center">
                                                <img src="<?php echo e(asset('images/tool_img3.png')); ?>" alt="tool image">
                                                <h3>Calculate your mortgage</h3>
                                           </div>
                                           <p>Figure out your estimated payments the easy way.</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3">
                                       <div class="tools">
                                           <h6>CALCULATOR</h6>
                                           <div class="main_center">
                                                <img src="<?php echo e(asset('images/tool_img3.png')); ?>" alt="tool image">
                                                <h3>Should You Refinance</h3>
                                           </div>
                                           <p>Calculate how much you can save by refinancing</p>
                                       </div>
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="loan_type_link">
                                <h3>Mortgage rates by loan type</h3>
                                <div class="row">
                                    <div class="col-md-4 p-0">
                                        <div class="loan_type_list">
                                            <ul>
                                                <li><a href="">30-year fixed mortgage rates</a></li>
                                                <li><a href="">10-year fixed mortgage rates</a></li>
                                                <li><a href="">3/1 arm mortgage rates</a></li>
                                                <li><a href="">Investment property mortgage rates</a></li>
                                                <li><a href="">Va mortgage rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4 p-0">
                                        <div class="loan_type_list">
                                            <ul>
                                                <li><a href="">30-year fixed mortgage rates</a></li>
                                                <li><a href="">10-year fixed mortgage rates</a></li>
                                                <li><a href="">3/1 arm mortgage rates</a></li>
                                                <li><a href="">Investment property mortgage rates</a></li>
                                                <li><a href="">Va mortgage rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4 p-0">
                                        <div class="loan_type_list">
                                            <ul>
                                                <li><a href="">30-year fixed mortgage rates</a></li>
                                                <li><a href="">10-year fixed mortgage rates</a></li>
                                                <li><a href="">3/1 arm mortgage rates</a></li>
                                                <li><a href="">Investment property mortgage rates</a></li>
                                                <li><a href="">Va mortgage rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="loan_type_link">
                                <h3>Mortgage and refinance rates by state</h3>
                                <div class="row">
                                    <div class="col-md-4 p-0">
                                        <div class="loan_type_list">
                                            <ul>
                                                <li><a href="">30-year fixed mortgage rates</a></li>
                                                <li><a href="">10-year fixed mortgage rates</a></li>
                                                <li><a href="">3/1 arm mortgage rates</a></li>
                                                <li><a href="">Investment property mortgage rates</a></li>
                                                <li><a href="">Va mortgage rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4 p-0">
                                        <div class="loan_type_list">
                                            <ul>
                                                <li><a href="">30-year fixed mortgage rates</a></li>
                                                <li><a href="">10-year fixed mortgage rates</a></li>
                                                <li><a href="">3/1 arm mortgage rates</a></li>
                                                <li><a href="">Investment property mortgage rates</a></li>
                                                <li><a href="">Va mortgage rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4 p-0">
                                        <div class="loan_type_list">
                                            <ul>
                                                <li><a href="">30-year fixed mortgage rates</a></li>
                                                <li><a href="">10-year fixed mortgage rates</a></li>
                                                <li><a href="">3/1 arm mortgage rates</a></li>
                                                <li><a href="">Investment property mortgage rates</a></li>
                                                <li><a href="">Va mortgage rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo e(asset('js/custom.js')); ?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            var p = $("#exampleButton").popover({
                trigger : 'hover',
            });
        // p.on("show.bs.popover", function(e){
        // p.data("bs.popover").tip().css({"max-width": "500px"});
        // });
        });
   
    $('input[name="Mortgages"]').on('change',function(){
        var get_val = $('input[name="Mortgages"]:checked').val();
        if(get_val == "Rocket Mortgages"){
            $("#country").html("<option value='Canada'>Canada</option>");
            $("#credit_score").html("<option value='720 or above'>720 or above</option>");
            $("#purchase_price").html("<option value='Minimum 10k+'>Minimum 10k+</option>");
            $("#down_payment").html("<option value='Minimum of 10% of purchase price'>Minimum of 10% of purchase price</option>");
            $(".intrest_rate").children('h5').css('display','none').after("<div class='bars4'><span></span><span></span><span></span><span></span><span></span></div>");
            $(".half_top_sec_logo").children('img').css('display','none').after("<div class='bars_logo'><span></span><span></span><span></span><span></span><span></span></div>");
            $(".half_top_sec_logo").children('p').css('display','none').after("<div class='bars_p'><span></span><span></span><span></span><span></span><span></span></div>");

            
            $(function(){
                setTimeout(function(){
                    $(".bars4").hide();
                    $(".bars_logo").hide();
                    $(".bars_p").hide();
                    $(".intrest_rate").children('h5').css('display','block');
                    $(".half_top_sec_logo").children('img').css('display','block');
                    $(".half_top_sec_logo").children('p').css('display','block');
                },4000);
            });
            
        }
        if(get_val == "Premium Mortgages"){
            $("#country").html("<option value='United States'>United States</option>");
            $("#credit_score").html("<option value='760+'>760+</option>");
            $("#purchase_price").html("<option value='Minimum 100k+'>Minimum 100k+</option>");
            $("#down_payment").html("<option value='Minimum of 20% of purchase price'>Minimum of 20% of purchase price</option>");
            $(".intrest_rate").children('h5').css('display','none').after("<div class='bars4'><span></span><span></span><span></span><span></span><span></span></div>");
            $(".half_top_sec_logo").children('img').css('display','none').after("<div class='bars_logo'><span></span><span></span><span></span><span></span><span></span></div>");
            $(".half_top_sec_logo").children('p').css('display','none').after("<div class='bars_p'><span></span><span></span><span></span><span></span><span></span></div>");
            
            
            $(function(){
                setTimeout(function(){
                    $(".bars4").hide();
                    $(".bars_logo").hide();
                    $(".bars_p").hide();
                    $(".intrest_rate").children('h5').css('display','block');
                    $(".half_top_sec_logo").children('img').css('display','block');
                    $(".half_top_sec_logo").children('p').css('display','block');
                },4000);
            });
        }
    });

    </script>
</body>

</html><?php /**PATH D:\Server\htdocs\mortgage-system\resources\views/mortgage-system.blade.php ENDPATH**/ ?>